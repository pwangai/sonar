# GitLab CI template for SonarQube

This project implements a GitLab CI/CD template to continuously inspect Wikimedia Gitlab projects with [SonarQube](https://docs.sonarqube.org/latest/). This is an initiative of the [Code Health Group](https://www.mediawiki.org/wiki/Code_Health_Group/projects/Code_Health_Metrics)

## Usage

In order to include this template in your java project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: "repos/ci-tools/sonarqube"
    ref: 'v0.1.0'
    file: "templates/maven-sonar-ci.yml"
```

This is a WIP, the goal is to have templates for all languages.
